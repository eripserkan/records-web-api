FROM node:16-alpine as build

WORKDIR /usr/
COPY package*.json ./
COPY tsconfig.json .
RUN npm install
COPY src ./src
RUN npm run build

FROM node:16-alpine
WORKDIR /usr
COPY package*.json ./
COPY --from=build /usr/build .
RUN npm ci --only=production
RUN npm install pm2 -g
CMD ["pm2-runtime","server.js"]