/* eslint-disable import/first */
import * as dotenv from 'dotenv';
const envFile = process.env.NODE_ENV === 'testing' ? `.env.testing` : '.env';
dotenv.config({ path: envFile });

import { mongooseConfig } from './mongoose';

const isProduction = process.env.NODE_ENV === 'production';
const port = process.env.PORT || '3000';
const host = process.env.HOST || 'localhost';

export default { isProduction, host, port, mongooseConfig };
