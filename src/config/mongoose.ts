const mongooseConfig = {
  databaseUrl: process.env.MONGO_DB_URL,
};
export { mongooseConfig };
