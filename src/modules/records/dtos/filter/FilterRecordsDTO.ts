export interface FilterRecordsDTO {
  startDate: Date;
  endDate: Date;
  minCount: number;
  maxCount: number;
}
