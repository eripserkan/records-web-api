import { ResponseCode } from '../../../../shared/domain/ResponseCode';
import { RecordDTO } from '../RecordDTO';

export interface FilterRecordsResponseDTO {
  code: ResponseCode;
  message: string;
  records: Array<RecordDTO>;
}
