export interface RecordDTO {
  key: string;
  createdAt: Date;
  totalCount: number;
}
