import { Record } from '../domain/entities/Record';
import { RecordDTO } from '../dtos/RecordDTO';

export class RecordMap {
  public static toDomain(raw: any): Record {
    return new Record({
      value: raw.value,
      createdAt: raw.createdAt,
      counts: raw.counts,
      key: raw.key,
    });
  }

  public static toDTO(record: Record): RecordDTO {
    return {
      key: record.key,
      totalCount: record.totalCount,
      createdAt: record.createdAt,
    };
  }
}
