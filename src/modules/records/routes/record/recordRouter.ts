import { Router } from 'express';
import { RecordService } from '../../domain/services/RecordService';
import { RecordMongooseRepository } from '../../domain/infra/implementations/RecordMongooseRepository';
import { RecordController } from '../../controllers/RecordController';
import { validationMiddleware } from '../../../../shared/infra/http';
import { filterRouteValidations } from './recordRouterValidations';

const recordRouter = Router();

const recordController = new RecordController(new RecordService(new RecordMongooseRepository()));

recordRouter.post(
  '/filter',
  ...filterRouteValidations,
  validationMiddleware,
  recordController.filter.bind(recordController)
);

export { recordRouter };
