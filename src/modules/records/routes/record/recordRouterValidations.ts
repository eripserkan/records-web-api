import { body } from 'express-validator';

const filterRouteValidations = [
  body('startDate')
    .isDate({
      format: 'YYYY-MM-DD',
    })
    .withMessage('startDate should be a date'),
  body('endDate')
    .isDate({
      format: 'YYYY-MM-DD',
    })
    .withMessage('endDate should be a date'),
  body('minCount').isNumeric().withMessage('minCount should be numeric'),
  body('maxCount').isNumeric().withMessage('maxCount should be numeric'),
];

export { filterRouteValidations };
