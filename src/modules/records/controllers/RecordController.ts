import { Controller } from '../../../shared/controllers/Controller';
import { NextFunction, Request, Response } from 'express';
import { IRecordService } from '../domain/services/IRecordService';

export class RecordController implements Controller {
  private service: IRecordService;

  constructor(recordService: IRecordService) {
    this.service = recordService;
  }

  async filter(req: Request, res: Response, next: NextFunction) {
    let response;
    try {
      response = await this.service.filter({
        startDate: new Date(req.body.startDate),
        endDate: new Date(req.body.endDate),
        minCount: req.body.minCount,
        maxCount: req.body.maxCount,
      });
    } catch (e) {
      console.log({ e });
      next(e);
    }

    return res.json(response).status(200);
  }
}
