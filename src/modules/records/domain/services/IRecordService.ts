import { FilterRecordsDTO } from '../../dtos/filter/FilterRecordsDTO';
import { FilterRecordsResponseDTO } from '../../dtos/filter/FilterRecordsResponseDTO';

export interface IRecordService {
  filter(req: FilterRecordsDTO): Promise<FilterRecordsResponseDTO>;
}
