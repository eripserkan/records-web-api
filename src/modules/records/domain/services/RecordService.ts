import { DomainService } from '../../../../shared/domain/DomainService';
import { IRecordService } from './IRecordService';
import { IRecordRepository } from '../infra/IRecordRepository';
import { ResponseCode } from '../../../../shared/domain/ResponseCode';
import { FilterRecordsDTO } from '../../dtos/filter/FilterRecordsDTO';
import { FilterRecordsResponseDTO } from '../../dtos/filter/FilterRecordsResponseDTO';
import { RecordMap } from '../../mappers/RecordMap';

export class RecordService implements DomainService, IRecordService {
  repository: IRecordRepository;

  constructor(recordRepository: IRecordRepository) {
    this.repository = recordRepository;
  }

  async filter(req: FilterRecordsDTO): Promise<FilterRecordsResponseDTO> {
    const records = await this.repository.filter(req);

    return {
      code: ResponseCode.OK,
      message: 'Success!!',
      records: records.map((record) => RecordMap.toDTO(record)),
    };
  }
}
