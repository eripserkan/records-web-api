export interface RecordProps {
  key: string;
  createdAt: Date;
  counts: Array<number>;
  value: string;
}
export class Record {
  props: RecordProps;

  get key(): string {
    return this.props.key;
  }

  get createdAt(): Date {
    return this.props.createdAt;
  }

  get counts(): Array<number> {
    return this.props.counts;
  }

  get value(): string {
    return this.props.value;
  }

  get totalCount(): number {
    return this.counts.reduce((a, b) => a + b, 0);
  }

  constructor(props: RecordProps) {
    this.props = props;
  }
}
