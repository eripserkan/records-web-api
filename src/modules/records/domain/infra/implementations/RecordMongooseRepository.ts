import { IRecordRepository } from '../IRecordRepository';

import { Record } from '../../entities/Record';
import RecordModel from '../../../../../shared/domain/models/RecordModel';
import { RecordMap } from '../../../mappers/RecordMap';
import { FilterRecordsDTO } from '../../../dtos/filter/FilterRecordsDTO';

export class RecordMongooseRepository implements IRecordRepository {
  async filter(req: FilterRecordsDTO): Promise<Array<Record>> {
    const documents = await RecordModel.aggregate([
      {
        $project: {
          key: 1,
          value: 1,
          createdAt: 1,
          counts: 1,
          totalCount: { $sum: '$counts' },
        },
      },
      {
        $match: {
          createdAt: { $gte: req.startDate, $lte: req.endDate },
          totalCount: { $gt: req.minCount, $lt: req.maxCount },
        },
      },
    ]);
    return documents.map((document) => RecordMap.toDomain(document));
  }
}
