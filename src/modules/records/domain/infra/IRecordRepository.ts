import { Record } from '../entities/Record';
import { FilterRecordsDTO } from '../../dtos/filter/FilterRecordsDTO';

export interface IRecordRepository {
  filter(req: FilterRecordsDTO): Promise<Array<Record>>;
}
