import * as mongoose from 'mongoose';

const scheme = new mongoose.Schema({
  key: String,
  createdAt: Date,
  counts: [Number],
  value: String,
});

const RecordModel = mongoose.model('Record', scheme);

export default RecordModel;
