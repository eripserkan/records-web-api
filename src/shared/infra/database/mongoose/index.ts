import config from '../../../../config';
import mongoose from 'mongoose';

async function connect() {
  await mongoose.connect(config.mongooseConfig.databaseUrl!!);
}

connect().catch((err) => {
  console.log('Cannot connect database: ' + err);
  process.exit(-1);
});
