import express from 'express';
import { recordRouter } from '../../../../modules/records/routes';

const v1Router = express.Router();

v1Router.get('/', (req, res) => {
  return res.json({ message: 'Up!' });
});

v1Router.use('/records', recordRouter);

export { v1Router };
