import config from '../../../config';
import express, { Application } from 'express';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';
import compression from 'compression';
import { v1Router } from './api/v1';

const app: Application = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
  cors({
    origin: '*',
  })
);
app.use(compression());
app.use(morgan('combined'));
app.use(helmet());

app.use('/api/v1', v1Router);

app.use(function (req: express.Request, res: express.Response) {
  res.status(404).json({ msg: 'Not found!' });
});

app.use(function (err: Error, req: express.Request, res: express.Response, next: express.NextFunction) {
  let msg = 'Something broken!';
  if (!config.isProduction) {
    msg = err.message;
  }
  res.json({ msg }).status(500);
});

export default app;
