import config from './config';
import http from 'http';
import app from './shared/infra/http/app';
import './shared/infra/database/mongoose';

const server = http.createServer(app);

const listenOn = `${config.host}:${config.port}`;

server.listen(parseInt(config.port, 10), config.host, () => {
  console.log(`Server started on ${listenOn}`);
});
