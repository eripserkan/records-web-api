/**
 * Integration tests for record endpoints
 * @group integration
 * @group record
 */

import request from 'supertest';
import app from '../../shared/infra/http/app';
import { RecordDTO } from '../../modules/records/dtos/RecordDTO';
import * as faker from 'faker';
import RecordModel from '../../shared/domain/models/RecordModel';
import '../../shared/infra/database/mongoose';

describe('Record Routes', () => {
  beforeEach(async () => {
    RecordModel.deleteMany();
  });

  afterEach(() => {});

  describe('/filter', () => {
    it('should return 400 status with errors if request body invalid', async () => {
      expect.hasAssertions();
      const result = await request(app).post('/api/v1/records/filter').send({});

      expect(result.statusCode).toEqual(400);
      expect(result.body.errors).toHaveLength(4);
    });

    it('should return filtered records', async () => {
      // arrange
      for (let i = 0; i < 10; i++) {
        await RecordModel.create({
          value: 'Holaa',
          createdAt: faker.date.between('2010-01-01', '2020-01-01'),
          counts: [faker.datatype.number({ min: 1000, max: 5000 }), faker.datatype.number({ min: 100, max: 5000 })],
          key: 'saKajoODQWMll',
        });
      }
      // create a filter without faker to expect at least minimum 1 record in response
      await RecordModel.create({
        value: 'Holaa',
        createdAt: new Date('2016-01-27'),
        counts: [2700],
        key: 'saKajoODQWMll',
      });
      const filter = {
        startDate: '2016-01-26',
        endDate: '2018-02-02',
        minCount: 2700,
        maxCount: 3000,
      };

      // act
      const result = await request(app).post('/api/v1/records/filter').send(filter);

      // assert
      expect(result.statusCode).toEqual(200);
      expect(result.body.records.length).toBeGreaterThanOrEqual(1);
      result.body.records.forEach((record: RecordDTO) => {
        expect(record.totalCount).toBeGreaterThanOrEqual(filter.minCount);
        expect(record.totalCount).toBeLessThanOrEqual(filter.maxCount);
        expect(new Date(record.createdAt).getTime()).toBeGreaterThanOrEqual(new Date(filter.startDate).getTime());
        expect(new Date(record.createdAt).getTime()).toBeLessThanOrEqual(new Date(filter.endDate).getTime());
      });
    });
  });
});
