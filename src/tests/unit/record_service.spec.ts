/**
 * RecordService class unit tests
 * @group unit
 * @group record
 */

import { ResponseCode } from '../../shared/domain/ResponseCode';
import { RecordService } from '../../modules/records/domain/services/RecordService';
import { FilterRecordsDTO } from '../../modules/records/dtos/filter/FilterRecordsDTO';
import { Record } from '../../modules/records/domain/entities/Record';
import { IRecordRepository } from '../../modules/records/domain/infra/IRecordRepository';

class DummyRecordRepository implements IRecordRepository {
  filter(req: FilterRecordsDTO): Promise<Array<Record>> {
    return Promise.resolve([
      new Record({
        value: 'Holaa',
        createdAt: new Date(),
        counts: [1, 2],
        key: 'saKajoODQWMll',
      }),
    ]);
  }
}

describe('Record Service', () => {
  it('should return filtered records', async () => {
    expect.hasAssertions();
    const recordService = new RecordService(new DummyRecordRepository());

    const dto = await recordService.filter({
      startDate: new Date(),
      endDate: new Date(),
      minCount: 0,
      maxCount: 1000,
    });

    expect(dto.code).toEqual(ResponseCode.OK);
    expect(dto.records).toHaveLength(1);
  });
});
