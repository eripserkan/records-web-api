/**
 * RecordMap class unit tests
 * @group unit
 * @group record
 */

import { RecordMap } from '../../modules/records/mappers/RecordMap';
import { Record } from '../../modules/records/domain/entities/Record';

describe('RecordMap', () => {
  it('should convert raw to domain model', () => {
    expect.hasAssertions();
    // arrange
    const raw = {
      value: 'Holaa',
      createdAt: new Date().toISOString(),
      counts: [1, 2],
      key: 'saKajoODQWMll',
    };
    // act
    const record = RecordMap.toDomain(raw);
    // assert
    expect(record.props).toEqual(raw);
  });

  it('should convert domain model to dto', () => {
    expect.hasAssertions();
    // arrange
    const record = new Record({
      value: 'Holaa',
      createdAt: new Date(),
      counts: [1, 2],
      key: 'saKajoODQWMll',
    });
    // act
    const dto = RecordMap.toDTO(record);
    // assert
    expect(dto.key).toBe(record.key);
    expect(dto.createdAt).toBe(record.createdAt);
    expect(dto.totalCount).toBe(record.totalCount);
  });
});
