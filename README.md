## Table of contents
- [Table of contents](#table-of-contents)
- [General Info](#general-info)
- [Features](#features)
- [Technologies](#technologies)
- [Architectural Decisions](#architectural-decisions)
- [Installation](#installation)
- [Development](#development)
- [Run With Docker](#run-with-docker)
- [Web Api](#web-api)
    - [Filter records](#filter-records)
        - [Request](#request)
        - [Response](#response)
- [License](#license)

## General Info
This is a basic rest api which can filter records saved on mongodb.

App is live on [http://records-api.serkanerip.com/.](http://records-api.serkanerip.com/)

## Features

* Filter records

## Technologies

* Nodejs
    * Express for request handling
    * Nodejs builtin http package for serving
    * Jest for unit and integration testing(with supertest)
* Docker
* Mongodb

## Architectural Decisions

* I used layered architecture for this project, but for directory structure I structured like DDD architecture.
* Different directories for all the different modules(=domains).
* I try to keep it simple and readable.

### Database
* Mongodb

### Deployment

* App runs on an ec2 machine in aws with docker

## Installation

```bash
git clone git@gitlab.com:eripserkan/records-web-api.git
```

## Development

### First of all

**Create env file from example**

```bash
cp .env.example .env
```

```bash
npm run install // installing dependencies

npm run start // start nodemon

npm run build // build

npm run test:unit // run unit tests
npm run test:integration // run integration tests
```

## Run With Docker
If you don't know how to install docker read the [docs.](https://docs.docker.com/engine/install/)

```
 docker run -d -p 3000:3000 -e MONGO_DB_URL="YOUR_MONGODB_URL" -e PORT=3000 -e HOST=0.0.0.0  serkanerip/records-web-api
```

## Web Api

### Filter records

#### Request
[**POST**] /api/v1/records/filter
```bash
$ curl --location --request POST 'http://localhost:3000/api/v1/records/filter' \
--header 'Content-Type: application/json' \
--data-raw '{
  "startDate": "2016-01-26",
  "endDate": "2018-02-02",
  "minCount": 2700,
  "maxCount": 3000
}'
```
#### Response
```json
{
  "code": 0,
  "message": "Success!!",
  "records": [
    {
      "key": "ibfRLaFT",
      "totalCount": 2892,
      "createdAt": "2016-12-25T16:43:27.909Z"
    },
    {
      "key": "pxClAvll",
      "totalCount": 2772,
      "createdAt": "2016-12-19T10:00:40.050Z"
    }
  ]
}
```

## License

The MIT License  2021 Serkan Erip
